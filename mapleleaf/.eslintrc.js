module.exports = {
  root: true,
  "extends": [
    // "standard",
    "plugin:vue/base"
  ],
  "env": {
    "node": true,
    "browser": true,
    "commonjs": true,
    "es6": true
  },
  //extends是扩展插件的意思，2个插件Vue必须安装！
  "extends": [// 此项是用来配置vue.js风格，就是说写代码的时候要规范的写，如果你使用vs-code我觉得应该可以避免出错
    "plugin:vue/essential",
    //这里的插件是：eslint的（eslint-config-standard和eslint-plugin-standard及相关依赖）
    "standard"
  ],
  "parserOptions": {
    "parser": "babel-eslint"//此项是用来指定eslint解析器的，解析器必须符合规则，babel-eslint解析器是对babel解析器的包装使其与ESLint解析
  },
  "rules": {
    //这里自定义规则，规则地址:
    'no-console': 'off',
    "space-before-function-paren": 0,
    "no-multiple-empty-lines": [1, { "max": 2 }],//空行最多不能超过2行
    "no-mixed-spaces-and-tabs": ["error", "smart-tabs"],//当 tab 是为了对齐，允许混合使用空格和 tab。
    "semi": 0,//必须分号结束！
    "indent": 0, //tab键4个空格
    "dot-location": ["error", "object"],
    "camelcase": ["error", { "allow": ["aa_bb"] }],
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
  },
  "globals": {
    "err": true
  }
}
