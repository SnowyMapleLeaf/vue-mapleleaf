import * as CategoryApi from './apis/CategoryApi'
import * as GoodsApi from './apis/GoodsApi'
import * as MenuApi from './apis/MenuApi'
// 默认导出
export default {
    CategoryApi,
    GoodsApi,
    MenuApi
}
