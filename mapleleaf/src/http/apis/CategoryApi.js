import reuest from '../request'

/*
 * 分类
 */

// 获取分类列表
export const GetCategoryList = (data) => {
    return reuest({
        url: 'CateGory/GetCategoryList',
        method: 'GET',
        params: data
    });
};
// 获取分类列表(树)
export const GetCategoryTree = (data) => {
    return reuest({
        url: 'CateGory/GetCategoryTree',
        method: 'GET',
        params: data
    });
};
// 保存分类
export const SaveCateGoryForm = (data) => {
    return reuest({
        url: 'CateGory/SaveCategoryForm',
        method: 'POST',
        data
    });
};
// 获取分类列表
export const DeleteCategory = (data) => {
    return reuest({
        url: 'CateGory/DeleteCategory',
        method: 'Get',
        params: data
    });
};
