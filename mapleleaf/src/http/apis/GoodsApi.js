import reuest from '../request'
/*
 * 商品
 */

// 获取分类列表
export const GetGoodsList = (data) => {
    return reuest({
        url: 'Goods/GetGoodsList',
        method: 'GET',
        params: data
    });
};

// 导入分类
export const ImportGoodsData = (data) => {
    return reuest({
        url: 'Goods/ImportGoodsData',
        method: 'POST',
        data,
        headers: { 'Content-type': 'multipart/form-data' }
    });
};
// 保存分类
export const SaveGoodsData = (data) => {
    return reuest({
        url: 'Goods/SaveGoodsData',
        method: 'POST',
        data
    });
};
