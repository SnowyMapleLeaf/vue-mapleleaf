import reuest from '../request'
/*
 * 菜单
 */
// 获取菜单列表
export const GetMenuList = (data) => {
    return reuest({
        url: 'Menu/GetMenuList',
        method: 'GET',
        params: data
    });
};
// 获取菜单列表
export const GetMenuTree = (data) => {
    return reuest({
        url: 'Menu/GetMenuTree',
        method: 'GET',
        params: data
    });
};
// 获取菜单列表
export const DeleteMenuFrom = (data) => {
    return reuest({
        url: 'Menu/DeleteMenuFrom',
        method: 'GET',
        params: data
    });
};
// 保存菜单数据
export const SaveMenuForm = (data) => {
    return reuest({
        url: 'Menu/SaveMenuForm',
        method: 'POST',
        data
    });
};
