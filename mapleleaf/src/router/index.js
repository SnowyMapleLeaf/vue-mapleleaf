import VueRouter from 'vue-router'
import Vue from 'vue'
import api from '../http/service'
import Layout from '../components/Layout'

Vue.use(VueRouter);


// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
const mainRoutes = {
    path: '/',
    component: Layout,
    name: 'Layout',
    hidden: true,
    children: [
        {
            path: '/Menu',
            component: () => import('../views/menu/MenuIndex'),
            name: 'Menu',
            hidden: true,
            meta: { title: '菜单' }
        }
    ]
};
const staticRouter = [{
    path: '/404',
    component: () => import('../views/404'),
    name: '404',
    hidden: true,
    meta: { title: '404' }
}, {
    path: '/login',
    component: () => import('../views/login'),
    name: 'login',
    hidden: true,
    meta: { title: '登录' }

}];

const router = new VueRouter({
    mode: 'history', // 去除访问路径“#”
    routes: staticRouter
});

var isNew = false;
router.beforeEach((to, from, next) => {
    if (!isNew) {
        const ItemData = localStorage.getItem('MenuList');// 从缓存中读取菜单数据
        if (ItemData === undefined || ItemData == null || ItemData.length === 0) {
            // 缓存不存在 调用API获取动态路由
            api.MenuApi.GetMenuTree().then(res => {
                if (res.Code === 200) {
                    isNew = true;
                    // 存储菜单信息
                    localStorage.setItem('MenuList', JSON.stringify(res.Result));
                    const routerData = formatterRouterData(res.Result);
                    mainRoutes.children = mainRoutes.children.concat(routerData);
                    console.log(JSON.stringify(mainRoutes));
                    router.addRoutes([mainRoutes, { path: '*', redirect: { name: '404' } }]);
                    next({ ...to, replace: true });
                }
            });
        } else {
            isNew = true;
            // 存储菜单信息
            const routerData = formatterRouterData(JSON.parse(ItemData));
            mainRoutes.children = mainRoutes.children.concat(routerData);
            console.log(JSON.stringify(mainRoutes));
            router.addRoutes([mainRoutes, { path: '*', redirect: { name: '404' } }]);
            next({ ...to, replace: true });
        }
    } else {
        next();
    }
})
const viewArr = require.context('../views', true, /.vue$/).keys();


// 格式化路由数据
function formatterRouterData(menuList = [], routerList = []) {
    if (menuList.length === 0) {
        return routerList;
    }
    for (let index = 0; index < menuList.length; index++) {
        const item = menuList[index];
        if (item.Children !== undefined && item.Children != null && item.Children.length > 0) {
            formatterRouterData(item.Children, routerList)
        } else {
            if (item.Url !== undefined && item.Url.length > 0 && viewArr.indexOf('.' + item.Url + '.vue') > -1) {
                const obj = {};
                obj.path = '/' + ((item.Url + '').split('/')[(item.Url + '').split('/').length - 1]);
                obj.component = () => import('../views' + item.Url);
                obj.name = item.Code;
                obj.hidden = true;
                obj.meta = { title: item.Name }
                routerList.push(obj);
            }
        }
    }
    return routerList;
}


export default router;
