export default [

    {
        id: 0,
        name: 'SystemSetting',
        icon: 'el-icon-setting',
        title: '系统设置',
        children: [
            {
                id: 1,
                name: 'Menu',
                icon: 'el-icon-menu',
                title: '菜单管理'
            }
        ]
    }

]
