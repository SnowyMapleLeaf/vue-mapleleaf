import Vue from 'vue';
import App from './App.vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router/index';
import api from './http';
import Treeselect from '@riophae/vue-treeselect'
import '@riophae/vue-treeselect/dist/vue-treeselect.css'

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(api);
Vue.use(Treeselect);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
